#!/bin/bash 

sudo apt-get update
sudo apt-get upgrade

sudo apt-get install libgstreamer1.0-0
sudo apt-get install gstreamer1.0-plugins-base gstreamer1.0-plugins-good 
sudo apt-get install gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly 
sudo apt-get install gstreamer1.0-libav gstreamer1.0-doc gstreamer1.0-tools

sudo apt-get install libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev
sudo apt-get install libfontconfig1-dev libfreetype6-dev libpng-dev
sudo apt-get install libcairo2-dev libjpeg-dev libgif-dev
sudo apt-get install libgstreamer-plugins-base1.0-dev