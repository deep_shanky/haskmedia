{-# LANGUAGE
    OverloadedStrings
#-}

-- | It consists of controls of a coursor in the media player
module UtilCursor where

import IOUtils
import TextUtils
import Control.Monad
import Data.Maybe
import qualified GI.Gdk
import qualified GI.Gtk
import Data.Text
import Data.IORef
import Data.Time.Clock.POSIX
import qualified FieldGuiObjects as FO

-- | It displays the cursor on the screen
showAndSetCursor::GI.Gtk.Window -> Maybe Text -> IO ()
showAndSetCursor mainWindow Nothing =
  fromJust <$> GI.Gtk.widgetGetWindow mainWindow >>=
  flip GI.Gdk.windowSetCursor (Nothing :: Maybe GI.Gdk.Cursor)
showAndSetCursor mainWindow (Just cursor) = do
  gdkWindow <- fromJust <$> GI.Gtk.widgetGetWindow mainWindow
  _cursor <- drawCursor gdkWindow cursor
  GI.Gdk.windowSetCursor gdkWindow _cursor

-- | It updates the time of the last cursor motion. Required in several further functions, related to updating
-- | volume, seeking the video etc.
cursorMotionFunc::FO.MainField -> [FO.MainField -> IO ()] -> GI.Gdk.EventMotion -> IO Bool
cursorMotionFunc mainAttrs@FO.MainField {
    FO.guifield = FO.GUIField {
        FO.mainWindow = mainWindow
    }
    , FO.iofield = FO.IOField {
            FO.isfullScreen  = fullScreen
          , FO.lastcursormov = lastcursormov
          , FO.videoinfo     = videoinfo
    }
  } cursormovecallback _ = do
    isfs    <- readIORef fullScreen
    vidInfo <- readIORef videoinfo
    showAndSetCursor mainWindow Nothing
    timeNow <- getPOSIXTime
    atomicWriteIORef lastcursormov (round timeNow)
    mapM_ (\ f -> f mainAttrs) cursormovecallback
    return False

-- | The main routine to handle the motion of mouse on the screen
controlCursor::FO.MainField -> [FO.MainField -> IO ()] -> IO ()
controlCursor mainAttrs@FO.MainField {
    FO.guifield = FO.GUIField {
        FO.fileSelectorButton = fileSelectorButton
      , FO.videoWidget        = videoWidget
      , FO.seekscale          = seekscale
    }
  } cursormovecallback = do
     void $ GI.Gtk.onWidgetMotionNotifyEvent fileSelectorButton cursorclicks
     void $ GI.Gtk.onWidgetMotionNotifyEvent videoWidget cursorclicks
     void $ GI.Gtk.onWidgetMotionNotifyEvent seekscale cursorclicks
     where
       cursorclicks::GI.Gdk.EventMotion -> IO Bool
       cursorclicks = cursorMotionFunc mainAttrs cursormovecallback
