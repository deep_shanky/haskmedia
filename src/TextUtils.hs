{-# LANGUAGE
    OverloadedStrings
#-}
-- | Manipulations to strings

module TextUtils where

import Data.Text
import System.FilePath
import Data.Int


-- | To convert enum type to int type
enumToInt32 :: (Enum a, Ord a) => a -> Int32
enumToInt32 enum = fromIntegral (fromEnum enum) :: Int32

-- | To fetch the file name from the path location where it is specified
fileNamefromPath :: Data.Text.Text -> Data.Text.Text
fileNamefromPath = Data.Text.pack . System.FilePath.takeFileName . Data.Text.unpack

-- | Checks if the text instance is empty
isTextEmpty :: Data.Text.Text -> Bool
isTextEmpty = Data.Text.null . Data.Text.strip

-- | String to signify if video widget is invalid.
invalidVideoWidget :: Data.Text.Text
invalidVideoWidget = "invalid-video-widget"

-- | returns the command xdg-open
getSomeCommand::String
getSomeCommand = do
  let command = "xdg-open"
  command

-- | Checks if the default attributes for a video are correctly set or not and return the same on the screen
checkVideoAttributes::(Bool, Int32, Int32, String, Bool, Bool) -> String
checkVideoAttributes (hasvideo, width, height, caption, addLocal, seekable) = do
  if (hasvideo == False && width == 800 && height == 600 && caption == "" && addLocal == False && seekable == False)
    then "set properly"
    else "set improperly"

-- | To convert int64 type to double
int64toDouble :: Int64 -> Double
int64toDouble i = fromIntegral i :: Double

-- | To convert double type to int64
doubleToInt64 :: Double -> Int64
doubleToInt64 d = fromIntegral (round d :: Int) :: Int64