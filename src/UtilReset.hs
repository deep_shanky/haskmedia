{-# LANGUAGE
    OverloadedStrings
#-}

-- | It focuses on resetting attributes
module UtilReset where

import Control.Monad
import Data.IORef
import Data.GI.Base.Properties
import Data.Int
import Text.Read
import Data.Maybe
import Data.List
import TextUtils
import qualified UtilCursor
import qualified UtilPlayVid
import qualified GI.Gtk
import qualified GI.Gst
import qualified FieldGuiObjects as FO
import qualified IOUtils


-- | reset main attributes
resetAttributes::FO.MainField -> IO ()
resetAttributes FO.MainField {
    FO.guifield     = guifield
    , FO.iofield    = iofield
    , FO.playfield  = playfield
  } = do
    resetIOField   iofield
    resetplayField playfield
    IOUtils.resetguiField  guifield

-- | It lets to reset the playfield elements
resetplayField::GI.Gst.Element -> IO ()
resetplayField playfield = do
  void $ GI.Gst.elementSetState playfield GI.Gst.StateNull
  void $ Data.GI.Base.Properties.setObjectPropertyDouble playfield "volume" 0.5
  void $ Data.GI.Base.Properties.setObjectPropertyString playfield "uri" (Just "")


-- | It lets to reset the attributes to IO
resetIOField::FO.IOField -> IO ()
resetIOField FO.IOField {
    FO.videoinfo      = videoinfo
    , FO.prevfilename = prevfilename
  } = do
    void $ resetVideoInfo videoinfo
    atomicWriteIORef prevfilename ""


-- | It lets to reset the stored information to a video back to default
saveVideoInfo::IORef FO.VideoField -> FO.VideoField -> IO ()
saveVideoInfo = atomicWriteIORef

-- | It lets to reset the GUI attributes 
resetVideoInfo::IORef FO.VideoField -> IO FO.VideoField
resetVideoInfo videoinfo = do
  let newVideoInfo = FO.VideoField {
     FO.hasvideo   = False
   , FO.width      = 800
   , FO.height     = 600
   , FO.caption    = ""
   , FO.addLocal   = False
   , FO.seekable   = False
  }
  saveVideoInfo videoinfo newVideoInfo
  return newVideoInfo
