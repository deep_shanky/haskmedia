{-# LANGUAGE
    OverloadedStrings
  , ForeignFunctionInterface
#-}

-- | Invoke ability to alter volume level and throw error while playing ambigous media
module UtilPlaybin where
import Data.Bits
import Data.IORef
import Data.Text
import Data.Maybe
import Control.Monad
import Foreign.C
import Foreign.Ptr
import qualified GI.GLib
import qualified GI.Gtk
import qualified GI.Gst
import Data.GI.Base.Properties
import Data.GI.Base.ManagedPtr
import qualified FieldGuiObjects as FO
import UtilReset
import TextUtils
import IOUtils
import UtilSeek
import System.FilePath
import Filesystem
import Filesystem.Path.CurrentOS
import UtilPlayVid


-- | Routine to handle volume button
volumeButtonhandler::FO.MainField -> IO ()
volumeButtonhandler mainAttrs@FO.MainField{
        FO.guifield     = FO.GUIField {
            FO.volumeBn = volumeBn
        }
      , FO.playfield = playfield
    } = do
        void $
            GI.Gtk.onScaleButtonValueChanged
                volumeBn $ setVolume playfield

-- | Set the level of volume
setVolume::GI.Gst.Element -> Double -> IO ()
setVolume playfield volume =
    void $ Data.GI.Base.Properties.setObjectPropertyDouble playfield "volume" volume

-- | main playbin handler
handleVidPlaybin::FO.MainField -> IO ()
handleVidPlaybin mainAttrs@FO.MainField {
          FO.playfield    = playfield
        , FO.playfieldbus = playfieldbus
    } = do
        void $
            GI.Gst.busAddWatch playfieldbus GI.GLib.PRIORITY_DEFAULT $
                messageBusRoutine mainAttrs

-- | To display set of messages like Open if there is no entry text or to throw Error in playing if an error occurs
messageBusRoutine::FO.MainField -> GI.Gst.Bus ->    GI.Gst.Message -> IO Bool
messageBusRoutine mainAttrs@FO.MainField {
        FO.guifield = FO.GUIField {
              FO.seekscale           = seekscale
            , FO.videolocation       = videolocation
            , FO.errorDialog         = errorDialog
            , FO.playpauseBn         = playpauseBn
            , FO.fileSelectorBnLabel = fileSelectorBnLabel
        }
        , FO.iofield = FO.IOField {
            FO.videoinfo = videoinfo
        }
        , FO.playfield = playfield
    } _ message = do
        messageTypes <- GI.Gst.getMessageType message
        let messageType = case messageTypes of
                [] -> GI.Gst.MessageTypeUnknown
                (msg:_) -> msg
        entryText <- GI.Gtk.entryGetText videolocation
        labelText <- GI.Gtk.labelGetText fileSelectorBnLabel

        when (messageType == GI.Gst.MessageTypeError && ((not . Data.Text.null) entryText || labelText /= "Open")) $ do
            (gError, text) <- GI.Gst.messageParseError message
            gErrorText     <- GI.Gst.gerrorMessage gError
            putStr ((Data.Text.unpack . Data.Text.unlines) [text, gErrorText])
            resetAttributes mainAttrs
            runErrorMessageDialog errorDialog $
                Data.Text.concat
                    ["Error in playing \"", entryText, "\"."]

        when (messageType == GI.Gst.MessageTypeBuffering) $ do
            percent     <- GI.Gst.messageParseBuffering message
            isPlaying   <- checkIsRunning playpauseBn
            if percent >= 100
                then do
                    when isPlaying $
                        void $
                            GI.Gst.elementSetState playfield GI.Gst.StatePlaying
                else do
                    void $ GI.Gst.elementSetState playfield GI.Gst.StatePaused
            return ()
        when (messageType == GI.Gst.MessageTypeStreamStart) $ do
            return ()

        when (messageType == GI.Gst.MessageTypeEos) $ do
            return ()
        return True