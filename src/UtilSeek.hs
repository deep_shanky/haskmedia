{-# LANGUAGE
    OverloadedStrings
#-}

-- | Consists of functions that enable user to easily seek the video to a desirable timeframe
module UtilSeek where

import FieldGuiObjects as FO
import Control.Monad
import Data.Word
import qualified GI.GLib
import qualified GI.GObject
import qualified GI.Gtk
import qualified GI.Gst
import IOUtils
import TextUtils
import Data.IORef
import Data.GI.Base.Signals
import Data.Maybe
import Data.Int


-- | This adjustes the speed of the video
speedSelector::FO.MainField -> IO ()
speedSelector mainAttrs@FO.MainField {
    guifield = GUIField {
      speedselector = speedselector
    }
  } =
    void $ GI.Gtk.onComboBoxChanged speedselector (seekZone mainAttrs Nothing Nothing)

-- | It fetches the time and position of the playfield
getslots::GI.Gst.Element -> IO (Maybe (Int64, Int64))
getslots playfield = do
  (time', time)         <- GI.Gst.elementQueryDuration playfield GI.Gst.FormatTime
  (position', position) <- GI.Gst.elementQueryPosition playfield GI.Gst.FormatTime
  if time' && position' && time > 0 && position >= 0
    then return (Just (time, position))
    else return Nothing

-- | Updates the seek scale on the gui window
updatescale::GI.Gst.Element ->  GI.Gtk.Scale ->  Data.GI.Base.Signals.SignalHandlerId -> IO Bool
updatescale playfield seekscale id = do
  scaleslot <- getslots playfield
  case scaleslot of
    (Just (time, position)) ->
      when (time > 0) $ do
        let completed = 100.0 * (int64toDouble position / int64toDouble time)
        GI.GObject.signalHandlerBlock seekscale id
        GI.Gtk.rangeSetValue seekscale completed
        GI.GObject.signalHandlerUnblock seekscale id
    _ -> return ()
  return True

-- | This seeks the audio/video to a different segement
seekFunc::FO.MainField -> IO ()
seekFunc mainAttrs@FO.MainField {
    guifield = GUIField {
      seekscale = seekscale
    }
    , playfield = playfield
  } = do
    id <- GI.Gtk.onRangeValueChanged seekscale ( valueModified mainAttrs )
    void $
      GI.GLib.timeoutAdd GI.GLib.PRIORITY_DEFAULT 41
        ( updatescale playfield seekscale id )

-- | It modifies the value of seekscale from the gui widget of the playback speed
valueModified::FO.MainField -> IO ()
valueModified mainAttrs@FO.MainField {
    guifield = GUIField {
      seekscale = seekscale
    }
    , iofield = IOField{
      videoinfo = videoinfo
    }
    , playfield = playfield
  } = do
    scaleslot <- getslots playfield
    case scaleslot of
      (Just (time, _)) -> do
        completed <- adjustVals 0.0 1.0 . flip (/) 100.0 . adjustVals 0.0 100.0 <$> GI.Gtk.rangeGetValue seekscale
        let position = adjustVals 0 time ( doubleToInt64 ( int64toDouble time * completed ) )
        seekZone mainAttrs (Just position) Nothing
        where
          adjustVals a1 a2 val
            | val < a1  = a1
            | val > a2  = a2
            | otherwise = val
      _ -> return ()

-- | It seeks the video from the current position to the desired position on the seek scale
fetchAndSeek::Double -> GI.Gst.Element -> Maybe Int64 -> IO ()
fetchAndSeek rate playfield desiredPosition = do
  slots <- getslots playfield
  case slots of
    (Just (_, currentPosition)) -> do
      let position  = fromMaybe currentPosition desiredPosition
      let seekattrs = GI.Gst.SeekFlagsFlush : [GI.Gst.SeekFlagsTrickmode | rate /= 1.0]
      void $
        GI.Gst.elementSeek playfield rate GI.Gst.FormatTime seekattrs GI.Gst.SeekTypeSet position GI.Gst.SeekTypeNone 0

    Nothing -> return ()

-- | The routine to adjust the playback rate and seek accordingly
seekWithRate::Double -> GI.Gst.Element -> Maybe Int64 -> Maybe Word32 -> IO ()
seekWithRate rate playfield desiredPosition delay =
  case delay of
    Just delay ->
      void $ GI.GLib.timeoutAdd GI.GLib.PRIORITY_DEFAULT
        (if delay < 0 then 0 else delay) $ do
          fetchAndSeek rate playfield desiredPosition
          return False
    Nothing -> fetchAndSeek rate playfield desiredPosition

-- | It fecthes the playback rate and adjust the seek functionality accordingly
seekZone::FO.MainField -> Maybe Int64 -> Maybe Word32 -> IO ()
seekZone mainAttrs@FO.MainField {
    guifield = GUIField {
      speedselector = speedselector
    }
    , iofield = IOField {
          videoinfo = videoinfo
    }
    , playfield = playfield
  } desiredPosition delay = do
    videoInfo <- readIORef videoinfo
    id        <- GI.Gtk.comboBoxGetActive speedselector
    let seekable    = FO.seekable videoInfo
    let hasVideo    = FO.hasvideo videoInfo
    case id of
      0 -> seekWithRate 0.5 playfield desiredPosition delay
      1 -> seekWithRate 0.75 playfield desiredPosition delay
      2 -> seekWithRate 1.0 playfield desiredPosition delay
      3 -> seekWithRate 1.25 playfield desiredPosition delay
      4 -> seekWithRate 1.5 playfield desiredPosition delay
      _ -> return ()

