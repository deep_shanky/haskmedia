{-# LANGUAGE
    OverloadedStrings
#-}

-- | It consists of neccesarry and essential functions relating to window.
module UtilWindow where

import GHC.Word
import Control.Monad
import Data.Maybe
import Data.Int
import Data.IORef
import Data.Time.Clock.POSIX
import GI.GLib
import qualified GI.Gdk
import qualified GI.Gtk
import qualified GI.Gst

import TextUtils
import qualified FieldGuiObjects as FO
import UtilPlayVid
import UtilSeek
import UtilCursor
import UtilReset
import IOUtils

-- | The main window routine. It controls the windowadjustment and destruction on closing
windowFunctions :: FO.MainField -> IO ()
windowFunctions mainField@FO.MainField {
    FO.guifield = FO.GUIField {
        FO.mainWindow  = mainWindow
      , FO.videoWidget = videoWidget
    }
    , FO.iofield = FO.IOField {
        FO.isfullScreen = isfullScreen
    }
    ,  FO.playfield = playfield
  }
  = do
    void $
      GI.GLib.timeoutAddSeconds
        GI.GLib.PRIORITY_DEFAULT 1 $
          windowAdjuster mainField
    void $
      GI.Gtk.onWidgetDestroy
        mainWindow $ windowsDestroyer playfield

-- | the routine to handle gui window destruction
windowsDestroyer :: GI.Gst.Element -> IO ()
windowsDestroyer playfield = do
  _ <- GI.Gst.elementSetState playfield GI.Gst.StateNull
  _ <- GI.Gst.objectUnref playfield
  GI.Gtk.mainQuit

-- | The routine to fetch video information and call further funcitons to fill the window with video
windowAdjuster :: FO.MainField -> IO Bool
windowAdjuster mainField@FO.MainField {
  FO.guifield = FO.GUIField {
        FO.mainWindow    = mainWindow
      , FO.controlBoxTop = controlBoxTop
    }
    , FO.iofield = FO.IOField {
        FO.videoinfo     = videoInfo
      , FO.lastcursormov = lastCursorMov
    }
    , FO.playfield = playfield
  }
  = do
    videoInformation <- readIORef videoInfo
    lastCursorMove   <- readIORef lastCursorMov
    timeNow          <- fmap round getPOSIXTime
    (_, playFieldState, _) <- GI.Gst.elementGetState playfield (fromIntegral GI.Gst.MSECOND :: GHC.Word.Word64)
    let isPlaying = GI.Gst.StatePlaying == playFieldState
    let delta = timeNow - lastCursorMove
    let isVideo = FO.hasvideo videoInformation
    fillWindowWithVideo mainField
    return True

-- | The handler to resize window and fill it with video
fillWindowWithVideo :: FO.MainField -> IO ()
fillWindowWithVideo FO.MainField {
    FO.guifield = guiObjects@FO.GUIField {
      FO.mainWindow = mainWindow
    }
    , FO.iofield = FO.IOField {
        FO.videoinfo    = videoInfo
      , FO.isfullScreen = isfullScreen
    }
  }
  = do
  isWindowFull     <- readIORef isfullScreen
  videoInformation <- readIORef videoInfo
  when(FO.hasvideo videoInformation && not isWindowFull) $ do
    (width, _) <- GI.Gtk.windowGetSize mainWindow
    maybeWindowSize <- getwindowsize guiObjects (fromIntegral width :: Int) videoInformation
    when(isJust maybeWindowSize) $ do
      let (windowWidth, windowHeight) = fromMaybe (0,0) maybeWindowSize
      resizeWindow guiObjects windowWidth windowHeight
  return ()

-- | The routine to set the gui window for video playback
setForVideoPlay :: FO.MainField -> Bool -> Bool -> Int32 -> Int32 -> IO ()
setForVideoPlay mainField@FO.MainField {
    FO.guifield =
      guiObjects@FO.GUIField
      {
          FO.mainWindow       = mainWindow
        , FO.videoWidget      = videoWidget
        , FO.controlBoxTop    = controlBoxTop
        , FO.controlBoxBottom = controlBoxBottom
        , FO.seekscale        = seekScale
        , FO.playpauseBn      = playPauseBn
        , FO.speedselector    = speedSelector
        , FO.playImage        = playImage
        , FO.pauseImage       = pauseImage
    }
  }
  isSeekable
  isWindowFull
  windowWidth
  windowHeight
  = do
    GI.Gtk.widgetShow videoWidget
    GI.Gtk.widgetShow playPauseBn
    windowMinimumHeight <- fetchMinimumHindowHeight controlBoxTop controlBoxBottom
    let windowMinimumWidth = 480
    GI.Gtk.widgetSetSizeRequest mainWindow windowMinimumWidth windowMinimumHeight
    setButton playPauseBn playImage pauseImage True
    unless isWindowFull $
      resizeWindow guiObjects windowWidth windowHeight
