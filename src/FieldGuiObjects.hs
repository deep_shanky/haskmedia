-- | This module defines the necesarry functionality for Gui. It has neccesary attributes for display of media

module FieldGuiObjects where

import qualified GI.Gtk
import qualified GI.Gst
import Data.IORef
import Data.Text


-- | This is the type class for data MainField with GUI attributes
data MainField = MainField
  {  guifield     :: GUIField
   , iofield      :: IOField
   , playfield    :: GI.Gst.Element
   , playfieldbus :: GI.Gst.Bus
  }

-- | This is the type class for data IOField with IO attributes
data IOField = IOField
  {  isfullScreen  :: IORef Bool
   , videoinfo     :: IORef VideoField
   , prevfilename  :: IORef Data.Text.Text
   , lastcursormov :: IORef Integer
  }

-- | This is the type class for data VideoField with video attributes
data VideoField = VideoField
  {  hasvideo :: Bool
   , width    :: Int
   , height   :: Int
   , caption  :: String
   , addLocal :: Bool
   , seekable :: Bool
  }

-- | This is the type class for data GUIField with file attributes
data GUIField = GUIField
  {  mainWindow           :: GI.Gtk.Window
   , infoLabel            :: GI.Gtk.Label
   , videolocation        :: GI.Gtk.Entry
   , fileSelectorBnLabel  :: GI.Gtk.Label
   , fileSelectorLabel    :: GI.Gtk.Label
   , fileSelectorDialog   :: GI.Gtk.Dialog
   , fileSelectorWidget   :: GI.Gtk.FileChooserWidget
   , fileSelectorButton   :: GI.Gtk.Button
   , fileSelectorCancelBn :: GI.Gtk.Button
   , fileSelectorOpenBn   :: GI.Gtk.Button
   , videoWidget          :: GI.Gtk.Widget
   , controlBoxTop        :: GI.Gtk.Box
   , controlBoxBottom     :: GI.Gtk.Box
   , seekscale            :: GI.Gtk.Scale
   , playpauseBn          :: GI.Gtk.Button
   , playImage            :: GI.Gtk.Image
   , pauseImage           :: GI.Gtk.Image
   , volumeBn             :: GI.Gtk.VolumeButton
   , widthselector        :: GI.Gtk.ComboBoxText
   , speedselector        :: GI.Gtk.ComboBoxText
   , errorDialog          :: GI.Gtk.MessageDialog
  }
