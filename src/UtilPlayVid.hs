{-# LANGUAGE
    OverloadedStrings
#-}

-- | Performs operatrions on running of media
module UtilPlayVid where

import qualified GI.Gdk
import qualified GI.Gtk
import qualified GI.Gst
import FieldGuiObjects as FO
import IOUtils
import TextUtils
import Control.Monad
import Data.Text

-- | Playbin handler
handlePlayBn::FO.MainField -> IO ()
handlePlayBn mainAttrs@FO.MainField {
    FO.guifield = FO.GUIField {
      FO.playpauseBn = playpauseBn
    }
  } = void $ GI.Gtk.onWidgetButtonReleaseEvent
               playpauseBn $ operateBn mainAttrs

-- | Figures out if state of media is running and returns false if neither of state exists
operateBn::FO.MainField -> GI.Gdk.EventButton -> IO Bool
operateBn FO.MainField {
      guifield = FO.GUIField {
              FO.playpauseBn = playpauseBn
            , FO.playImage   = playImage
            , FO.pauseImage  = pauseImage
            }
  , FO.playfield = playfield
  } _ = do
    running <- checkIsRunning playpauseBn
    if running
      then do
        setButton playpauseBn playImage pauseImage False
        void $ GI.Gst.elementSetState playfield GI.Gst.StatePaused
      else do
        setButton playpauseBn playImage pauseImage True
        void $ GI.Gst.elementSetState playfield GI.Gst.StatePlaying
    return False
