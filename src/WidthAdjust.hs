{-# LANGUAGE
    OverloadedStrings
#-}

-- | It consists of functions to mnake alterations in regard to width
module WidthAdjust where

import FieldGuiObjects as FO
import TextUtils
import UtilWindow
import IOUtils
import UtilReset
import Control.Monad
import Data.Text
import Data.IORef
import Data.Int
import Text.Read
import Data.Maybe
import qualified GI.Gtk
import qualified GI.GLib


-- | Enables to set the width of the window field
widthSelector::FO.MainField -> IO ()
widthSelector mainAttrs@FO.MainField {
    guifield = guiAttrs@FO.GUIField {
      widthselector = widthselector
    }
  } = do
  void $ GI.Gtk.onComboBoxChanged widthselector
      (vidSelector mainAttrs)

  void $ GI.GLib.timeoutAddSeconds GI.GLib.PRIORITY_DEFAULT 1
      (operateCustom mainAttrs)

-- | Enables to make a selection of width via drag drop mechanism
operateCustom :: FO.MainField -> IO Bool
operateCustom mainAttrs@FO.MainField {
    FO.guifield = guiAttrs@FO.GUIField {
        FO.mainWindow    = mainWindow
      , FO.widthselector = widthselector
    }
    , FO.iofield = FO.IOField{
        FO.videoinfo = videoinfo
    }
  } = do
  (windowwid, _)  <- GI.Gtk.windowGetSize mainWindow
  maybeWidth       <- fetchSelectedWidth widthselector

  let windowWidth = fromIntegral windowwid :: Int
  case maybeWidth of
    Just selectedWid ->
      when (windowWidth /= selectedWid) $
        GI.Gtk.comboBoxSetActive widthselector (-1)

    Nothing -> do
      let windowWidText = Data.Text.pack $ show windowWidth
      current <- GI.Gtk.comboBoxSetActiveId widthselector $ Just windowWidText
      if current
        then return ()
        else GI.Gtk.comboBoxSetActive widthselector (-1)
  return True


-- | Enables to select the video to be displayed on the current window
vidSelector::FO.MainField -> IO ()
vidSelector mainAttrs@FO.MainField {
    guifield = guiAttrs@FO.GUIField {
        mainWindow    = mainWindow
      , widthselector = widthselector
    }
    , iofield = FO.IOField{
        FO.videoinfo = videoinfo
    }
  } = do
  vidInfo            <- readIORef videoinfo
  desiredWindowWidth <- fetchDesiredWidth widthselector mainWindow
  maybesize          <- IOUtils.getwindowsize guiAttrs desiredWindowWidth vidInfo
  case maybesize of
    Nothing           -> do
      UtilReset.resetAttributes mainAttrs
    Just (width, height) -> IOUtils.resizeWindow guiAttrs width height
