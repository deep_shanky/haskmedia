{-# LANGUAGE OverloadedStrings #-}

-- | Incubate the methodology of selection of files 
module FileChoose where

import Control.Monad
import Data.Text
import Data.Maybe
import Data.Int
import Data.IORef
import qualified GI.Gdk
import FieldGuiObjects as FO
import qualified GI.Gtk
import qualified GI.Gst
import qualified Network.URI
import Data.GI.Base.Properties

import IOUtils
import TextUtils
import UtilPlayVid
import UtilPlaybin
import UtilWindow
import UtilReset

-- | Enable filling up the video entry box
videolocationHandler::FO.MainField -> IO ()
videolocationHandler mainAttrs@FO.MainField{
  FO.guifield = FO.GUIField {
      FO.videolocation = videolocation
    }
  } = do
    void $
      GI.Gtk.onEntryIconRelease
        videolocation $ \ _ _ ->
          GI.Gtk.entrySetText videolocation ""

-- | Routine to operate handlers on file selectors and changes
fileSelectorRoutine :: FO.MainField -> IO ()
fileSelectorRoutine mainAttrs@FO.MainField{
  FO.guifield = FO.GUIField {
        FO.fileSelectorButton = fileSelectorButton
      , FO.fileSelectorDialog = fileSelectorDialog
      , FO.fileSelectorWidget = fileSelectorWidget
    }
  }
  = do
  void $
    GI.Gtk.onWidgetButtonReleaseEvent
      fileSelectorButton $
        fileSelectorDialogHandler mainAttrs
  void $
    GI.Gtk.onFileChooserSelectionChanged
      fileSelectorWidget $
        fileSelectionHandler mainAttrs


-- | Rotine to handle file selector dialog button
fileSelectorDialogHandler :: FO.MainField -> GI.Gdk.EventButton -> IO Bool
fileSelectorDialogHandler FO.MainField{
  FO.guifield = FO.GUIField {
    FO.videolocation = videolocation
    , FO.fileSelectorDialog  = fileSelectorDialog
  }
  , FO.iofield = FO.IOField{
      FO.prevfilename = prevfilename
    }
  }
  _
  = do
  location <- GI.Gtk.entryGetText videolocation
  atomicWriteIORef prevfilename location
  response <- GI.Gtk.dialogRun fileSelectorDialog
  return True

-- | Routine to handle file selectior widget
fileSelectionHandler :: FO.MainField -> IO ()
fileSelectionHandler FO.MainField {
  FO.guifield = FO.GUIField{
      FO.fileSelectorWidget  = fileSelectorWidget
    , FO.videolocation = videolocation
  }
  , FO.iofield = FO.IOField {
        FO.videoinfo = videoinfo
    }
  }
  = do
  location <- GI.Gtk.fileChooserGetUri fileSelectorWidget
  case location of
    Nothing -> return ()
    Just loc -> do
      let currentlocation = Network.URI.unEscapeString $ Data.Text.unpack loc
      GI.Gtk.entrySetText
        videolocation $
          Data.Text.pack currentlocation

-- | Routine to operate handlers on dialog response
dialogResponseRoutine :: FO.MainField -> IO ()
dialogResponseRoutine mainAttrs@FO.MainField {
      FO.guifield = FO.GUIField{
          FO.fileSelectorButton = fileSelectorButton
        , FO.fileSelectorCancelBn = fileSelectorCancelBn
        , FO.fileSelectorDialog = fileSelectorDialog
      }
    }
  = do
   void $
    GI.Gtk.onDialogResponse
    fileSelectorDialog $
    fileSelectorResponseRoutine mainAttrs

-- | To save information about video
saveVidinf :: IORef FO.VideoField -> FO.VideoField -> IO ()
saveVidinf = atomicWriteIORef

-- | Return routine to reset previous video field on file selection, button click. It resets video play field and set corresponding window size, volume and video location. If a widget is invalid, it throws an error
-- | If file is not in the format of an audio/video, corresponding error is thrown on the screen.
fileSelectorResponseRoutine :: FO.MainField -> Int32 -> IO ()
fileSelectorResponseRoutine mainAttrs@FO.MainField{
  FO.guifield = guiAttrs@FO.GUIField {
        FO.mainWindow           = mainWindow
      , FO.videoWidget          = videoWidget
      , FO.widthselector        = widthselector
      , FO.videolocation        = videolocation
      , FO.fileSelectorBnLabel  = fileSelectorBnLabel
      , FO.volumeBn             = volumeBn
      , FO.errorDialog          = errorDialog
      , FO.fileSelectorDialog   = fileSelectorDialog
    }
    , FO.iofield = FO.IOField {
        FO.isfullScreen = isfullScreen
      , FO.videoinfo    = videoinfo
      , FO.prevfilename = prevfilename
    }
    , FO.playfield = playfield
  } id
  = do

    let defaultvideoAttributes = FO.VideoField {
      FO.hasvideo   = False
     , FO.width     = 800
     , FO.height    = 600
     , FO.caption   = ""
     , FO.addLocal  = False
     , FO.seekable  = False
    }

    GI.Gtk.widgetHide fileSelectorDialog
    if (enumToInt32 GI.Gtk.ResponseTypeOk == id)
      then do
        path <- GI.Gtk.entryGetText videolocation
        maybename <- setSelectorButtonlabel fileSelectorBnLabel path
        putStrLn (Data.Text.unpack(path))
        case maybename of
          (Just _) -> do
            let pathstring = Data.Text.unpack path
            currentvideoinfo   <- fromMaybe defaultvideoAttributes <$> getVideoInfo videoinfo pathstring
            windowdesiredwidth <- fetchDesiredWidth widthselector mainWindow
            maybesize          <- getwindowsize guiAttrs windowdesiredwidth currentvideoinfo

            case maybesize of
              Just (width, height) -> do
                videoWidgetName <- GI.Gtk.widgetGetName videoWidget
                if videoWidgetName == "invalid-video-widget"
                  then do
                    resetAttributes mainAttrs
                    runErrorMessageDialog
                      errorDialog "Unable to play video"
                  else do
                    isWindowFullScreen <- readIORef isfullScreen
                    setForVideoPlay mainAttrs (FO.seekable currentvideoinfo) isWindowFullScreen width height
                    saveVidinf videoinfo currentvideoinfo
                    resetplayField playfield
                    volume <- GI.Gtk.scaleButtonGetValue volumeBn
                    setPlaybinLocationAndVolume playfield pathstring volume
                    void $ GI.Gst.elementSetState playfield GI.Gst.StatePlaying
                    where
                      setVolume::GI.Gst.Element -> Double -> IO ()
                      setVolume playfield volume =
                        void $ Data.GI.Base.Properties.setObjectPropertyDouble playfield "volume" volume

                      setPlaybinLocator :: GI.Gst.Element -> Maybe String -> IO ()
                      setPlaybinLocator playbin (Just fileName) =
                        void $ Data.GI.Base.Properties.setObjectPropertyString playbin "uri" (Just $ pack fileName)
                      setPlaybinLocator playbin Nothing =
                        void $ Data.GI.Base.Properties.setObjectPropertyString playbin "uri" (Just "")

                      setPlaybinLocationAndVolume :: GI.Gst.Element -> Prelude.String -> Double -> IO ()
                      setPlaybinLocationAndVolume playbin fileName volume = do
                        setVolume playbin volume
                        setPlaybinLocator playbin (Just fileName)

              _ -> do
                resetAttributes mainAttrs
                runErrorMessageDialog errorDialog $ Data.Text.pack "File is not a audio/video."

          _ -> resetAttributes mainAttrs

    else do
      pathstring <- readIORef prevfilename
      fromMaybe defaultvideoAttributes <$>
        getVideoInfo videoinfo (Data.Text.unpack pathstring) >>=
          saveVideoInfo videoinfo
      _ <- setSelectorButtonlabel fileSelectorBnLabel pathstring
      GI.Gtk.entrySetText videolocation pathstring
