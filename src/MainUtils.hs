{-# LANGUAGE
    OverloadedStrings
#-}

-- | Invloves creation of several attributes
module MainUtils where

import FieldGuiObjects as FO
import Data.IORef
import System.Exit
import System.Process
import System.FilePath
import Control.Monad
import Control.Exception
import Text.Read
import Data.Maybe
import Data.GI.Base
import Data.GI.Base.Properties
import Data.List
import Data.Text
import Data.Int
import GI.GObject

import qualified IOUtils
import qualified TextUtils
import qualified FileChoose
import qualified UtilCursor
import qualified WidthAdjust
import qualified UtilWindow
import qualified UtilSeek
import qualified UtilReset
import qualified UtilPlaybin
import qualified UtilPlayVid

import qualified GI.Gdk
import qualified GI.Gtk
import qualified GI.Gst


-- | This build the complete frame of screen which would appear on the window
buildMainFrame :: GI.Gtk.IsBuilder build => build -> IO ()
buildMainFrame frameBuilder = do

  isfullscreen       <- newIORef False
  lastcursormov      <- newIORef 0
  prevfilename       <- newIORef ""

  let videofield     = createVideoAttributes (False, 800, 600, "", False, False)
  videoAttributes    <- newIORef videofield
  let ioAttributes   = createIOAttributes (isfullscreen, lastcursormov,
                prevfilename, videoAttributes)

  playfield      <- fromJust <$> GI.Gst.elementFactoryMake "playbin" (Just "MultimediaPlayerPlaybin")
  playfieldbus   <- fromJust <$> GI.Gst.elementGetBus playfield
  gtksink        <- GI.Gst.elementFactoryMake "gtksink" (Just "MultimediaPlayerGtkSink")

  videoWidgetfield <-
      case gtksink of
        Nothing   -> do
          putStrLn "Unable to create gtksink"
          drawingArea <- GI.Gtk.drawingAreaNew
          GI.Gtk.widgetSetName drawingArea "invalid-video-widget"
          GI.Gtk.unsafeCastTo GI.Gtk.Widget drawingArea
        Just gtksink ->
          fromJust <$> Data.GI.Base.Properties.getObjectPropertyObject gtksink "widget" GI.Gtk.Widget

  newwindow               <- IOUtils.getObject GI.Gtk.Window frameBuilder "window"
  fileSelectorButtonLabel <- IOUtils.getObject GI.Gtk.Label frameBuilder "file-chooser-dialog-b-label"
  fileSelectorLabel       <- IOUtils.getObject GI.Gtk.Label frameBuilder "file-chooser-dialog-label"
  infoLabel               <- IOUtils.getObject GI.Gtk.Label frameBuilder "info-dialog-label"
  videolocation           <- IOUtils.getObject GI.Gtk.Entry frameBuilder "video-location-entry"
  fileSelectorWidget      <- IOUtils.getObject GI.Gtk.FileChooserWidget frameBuilder "file-chooser-widget"
  videoWidget             <- IOUtils.getObject GI.Gtk.Box frameBuilder "video-widget-box"
  controlBoxTop           <- IOUtils.getObject GI.Gtk.Box frameBuilder "top-controls-box"
  controlBoxBottom        <- IOUtils.getObject GI.Gtk.Box frameBuilder "bottom-controls-box"
  seekscale               <- IOUtils.getObject GI.Gtk.Scale frameBuilder "seek-scale"
  fileSelectorDialog      <- IOUtils.getObject GI.Gtk.Dialog frameBuilder "file-chooser-dialog"
  fileSelectorButton      <- IOUtils.getObject GI.Gtk.Button frameBuilder "file-chooser-dialog-button"
  fileSelectorCancelBn    <- IOUtils.getObject GI.Gtk.Button frameBuilder "file-chooser-dialog-cancel-button"
  fileSelectorOpenBn      <- IOUtils.getObject GI.Gtk.Button frameBuilder "file-chooser-dialog-open-button"
  playpauseBn             <- IOUtils.getObject GI.Gtk.Button frameBuilder "play-pause-button"
  playImage               <- IOUtils.getObject GI.Gtk.Image frameBuilder "play-image"
  pauseImage              <- IOUtils.getObject GI.Gtk.Image frameBuilder "pause-image"
  errorDialog             <- IOUtils.getObject GI.Gtk.MessageDialog frameBuilder "error-message-dialog"
  volumeBn                <- IOUtils.getObject GI.Gtk.VolumeButton frameBuilder "volume-button"
  widthselector           <- IOUtils.getObject GI.Gtk.ComboBoxText frameBuilder "window-width-selection-combo-box-text"
  speedselector           <- IOUtils.getObject GI.Gtk.ComboBoxText frameBuilder "video-speed-selection-combo-box-text"

  GI.Gtk.dialogAddActionWidget fileSelectorDialog fileSelectorCancelBn (TextUtils.enumToInt32 GI.Gtk.ResponseTypeCancel)
  GI.Gtk.dialogAddActionWidget fileSelectorDialog fileSelectorOpenBn   (TextUtils.enumToInt32 GI.Gtk.ResponseTypeOk)

  Data.GI.Base.Properties.setObjectPropertyObject playfield "video-sink" gtksink
  Data.GI.Base.Properties.setObjectPropertyBool   playfield "force-aspect-ratio" True
  GI.Gtk.boxPackStart videoWidget videoWidgetfield True True 0
  GI.Gtk.widgetSetHexpand   videoWidgetfield True
  GI.Gtk.widgetSetVexpand   videoWidgetfield True
  GI.Gtk.widgetSetSensitive videoWidgetfield True

  let fieldGuiAttributes = FO.GUIField {
     mainWindow            = newwindow
   , infoLabel             = infoLabel
   , videolocation         = videolocation
   , fileSelectorBnLabel   = fileSelectorButtonLabel
   , fileSelectorLabel     = fileSelectorLabel
   , fileSelectorDialog    = fileSelectorDialog
   , fileSelectorWidget    = fileSelectorWidget
   , fileSelectorButton    = fileSelectorButton
   , fileSelectorCancelBn  = fileSelectorCancelBn
   , fileSelectorOpenBn    = fileSelectorOpenBn
   , videoWidget           = videoWidgetfield
   , controlBoxTop         = controlBoxTop
   , controlBoxBottom      = controlBoxBottom
   , seekscale             = seekscale
   , playpauseBn           = playpauseBn
   , playImage             = playImage
   , pauseImage            = pauseImage
   , volumeBn              = volumeBn
   , widthselector         = widthselector
   , speedselector         = speedselector
   , errorDialog           = errorDialog
  }

  let mainAttributes = createMainField (fieldGuiAttributes, ioAttributes, playfield, playfieldbus)

  UtilWindow.windowFunctions        mainAttributes
  FileChoose.fileSelectorRoutine    mainAttributes
  FileChoose.dialogResponseRoutine  mainAttributes
  FileChoose.videolocationHandler   mainAttributes
  UtilCursor.controlCursor          mainAttributes  [UtilWindow.fillWindowWithVideo]
  WidthAdjust.widthSelector         mainAttributes
  UtilPlaybin.handleVidPlaybin      mainAttributes
  UtilPlaybin.volumeButtonhandler   mainAttributes
  UtilSeek.seekFunc                 mainAttributes
  UtilSeek.speedSelector            mainAttributes
  IOUtils.errorHandler              mainAttributes
  UtilPlayVid.handlePlayBn          mainAttributes

  GI.Gtk.widgetShowAll newwindow
  GI.Gtk.main


-- | Develop attributes defined for video field to some intital default values
createVideoAttributes::(Bool, Int32, Int32, String, Bool, Bool) -> FO.VideoField
createVideoAttributes (hasvideo, width, height, caption, addLocal, seekable) = do
  let videoAttributes = FO.VideoField {
     FO.hasvideo  = False
   , FO.width     = 800
   , FO.height    = 600
   , FO.caption   = ""
   , FO.addLocal  = False
   , FO.seekable  = False
  }
  videoAttributes


-- | Creates attributes defined for IO
createIOAttributes::(IORef Bool, IORef Integer, IORef Text, IORef FO.VideoField)
  -> FO.IOField
createIOAttributes (isfullscreen, lastcursormov, prevfilename, videoAttributes) = do
  let ioAttributes = FO.IOField {
    FO.isfullScreen    = isfullscreen
    , FO.lastcursormov = lastcursormov
    , FO.prevfilename  = prevfilename
    , FO.videoinfo     = videoAttributes
  }
  ioAttributes

-- | Creates attributes defined for GUI
createMainField::(FO.GUIField, FO.IOField, GI.Gst.Element, GI.Gst.Bus) -> FO.MainField
createMainField (fieldGuiAttributes, ioAttributes, playfield, playfieldbus) = do
  let mainFieldAttributes = FO.MainField {
    FO.guifield       = fieldGuiAttributes
    , FO.iofield      = ioAttributes
    , FO.playfield    = playfield
    , FO.playfieldbus = playfieldbus
  }
  mainFieldAttributes