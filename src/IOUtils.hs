{-# LANGUAGE
    OverloadedStrings
#-}

-- | It has handler and builder functions for necessary adjustments
module IOUtils where

import Control.Monad
import qualified FieldGuiObjects as FO
import TextUtils
import qualified GI.Gdk
import qualified GI.Gtk
import qualified GI.Gst
import Data.Text
import Data.IORef
import Data.Int
import Text.Read
import Data.Maybe
import System.FilePath
import Data.List
import Control.Exception
import System.Exit
import System.Process
import Data.GI.Base
import Data.GI.Base.Properties
import GI.GObject

-- | This generates the builder
getObject::
    (GI.GObject.GObject retType, GI.Gtk.IsBuilder build) =>
    (Data.GI.Base.ManagedPtr retType -> retType) -> build -> String -> IO retType
getObject objectClass builder id = do
  built <- GI.Gtk.builderGetObject builder ( pack id )
  when (isNothing built) $
    putStrLn $ "[ERROR] could not build " ++ id ++ "."
  GI.Gtk.unsafeCastTo objectClass ( fromJust built )


-- | This is for handling any error
errorHandler::FO.MainField -> IO()
errorHandler FO.MainField {
    FO.guifield = FO.GUIField {
      FO.errorDialog = messageDialog
    }
  } = void (GI.Gtk.onDialogResponse messageDialog (responseHandler messageDialog) )


-- | This is for handling response to the errors
responseHandler::GI.Gtk.MessageDialog -> Int32 -> IO ()
responseHandler messageDialog _ = GI.Gtk.widgetHide messageDialog >>
    GI.Gtk.setMessageDialogText messageDialog "Error encountered"

-- | It throws dialog to error message
runErrorMessageDialog::GI.Gtk.MessageDialog -> Text -> IO ()
runErrorMessageDialog errorDialog text =
  GI.Gtk.setMessageDialogText errorDialog text >>
  void (GI.Gtk.dialogRun errorDialog)


-- | It aims at adjusting the width of the video
fetchSelectedWidth::GI.Gtk.ComboBoxText -> IO (Maybe Int)
fetchSelectedWidth widthselectortext = do
  id <- GI.Gtk.comboBoxGetActive widthselectortext
  if id == (-1)
    then return Nothing
    else do
      someStr <- Data.Text.unpack <$> GI.Gtk.comboBoxTextGetActiveText
                        widthselectortext
      return (readMaybe someStr :: Maybe Int)

-- | It aims at adjusting width to the desired amount given
fetchDesiredWidth::GI.Gtk.ComboBoxText -> GI.Gtk.Window -> IO Int
fetchDesiredWidth widthselectortext mainWindow = do
  maybeWidth <- fetchSelectedWidth widthselectortext
  case maybeWidth of
    Just selectedWidth -> do
      return selectedWidth
    _ -> do
      (windowWidth, _) <- GI.Gtk.windowGetSize mainWindow
      return (fromIntegral windowWidth :: Int)


-- | It returns the basic information about a video
getVideoInfo :: IORef FO.VideoField -> Prelude.String -> IO (Maybe FO.VideoField)
getVideoInfo videoInfoRef filePathName = do
    videoInfo <- readIORef videoInfoRef
    return $ Just videoInfo

-- | It fetches the size of the widget
fetchwidgetSize :: GI.Gtk.IsWidget w => w -> IO (Int32, Int32)
fetchwidgetSize widget = do
  rectangle <- GI.Gtk.widgetGetAllocation widget
  width     <- GI.Gdk.getRectangleWidth   rectangle
  height    <- GI.Gdk.getRectangleHeight  rectangle
  return (width, height)

-- | It evaluates the size of the current window
getwindowsize :: FO.GUIField -> Int -> FO.VideoField -> IO (Maybe (Int32, Int32))
getwindowsize
  FO.GUIField {
    FO.controlBoxTop = controlBoxTop
  } desireWidth FO.VideoField {
    FO.width = width
    , FO.height = height
  }
  = do
  topControlBoxIsVisible <- GI.Gtk.widgetGetVisible controlBoxTop
  controlBoxTopHeight <- GI.Gtk.widgetGetAllocation controlBoxTop >>= GI.Gdk.getRectangleHeight
  let videoWidthDouble = fromIntegral width :: Double
  let videoHeightDouble = fromIntegral height :: Double
  let ratio =
  	if videoWidthDouble <= 0.0
      then 0.0
      else videoHeightDouble/videoWidthDouble
  let desiredWidthDouble = fromIntegral desireWidth :: Double
  let topMargin =
  	if topControlBoxIsVisible
      then (fromIntegral controlBoxTopHeight :: Double)
      else 0.0
  let bottonMargin = 0.0
  let heightMargin = topMargin + (desiredWidthDouble * ratio) + bottonMargin
  return $ Just (fromIntegral desireWidth :: Int32, round heightMargin :: Int32)

-- | Resizes the height and width margin
resizeWindow :: FO.GUIField -> Int32 -> Int32 -> IO ()
resizeWindow
  FO.GUIField{
    FO.mainWindow = mainWindow
  }
  width
  heightMargin
  =
  GI.Gtk.windowResize mainWindow width (if heightMargin <= 0 then 1 else heightMargin)

-- | It fetches the minimun height size of the window
fetchMinimumHindowHeight:: GI.Gtk.Box -> GI.Gtk.Box -> IO Int32
fetchMinimumHindowHeight controlBoxTop controlBoxBottom = do
  (_, controlBoxTopHeight )    <- fetchwidgetSize controlBoxTop
  (_, controlBoxBottomHeight)  <- fetchwidgetSize controlBoxBottom
  bottomControlBoxMarginTop    <- GI.Gtk.widgetGetMarginTop controlBoxBottom
  bottomControlBoxMarginBottom <- GI.Gtk.widgetGetMarginBottom controlBoxBottom
  return $
    controlBoxBottomHeight
    + controlBoxBottomHeight
    + bottomControlBoxMarginBottom
    + bottomControlBoxMarginTop

-- | This is routines from fileselection
setSelectorButtonlabel :: GI.Gtk.Label -> Data.Text.Text -> IO (Maybe Data.Text.Text)
setSelectorButtonlabel fileChooserDialogButtonLabel filePathName = do
  let fileName = fileNamefromPath filePathName
  let fileNameEmpty = isTextEmpty fileName
  if fileNameEmpty
    then do
      GI.Gtk.labelSetText fileChooserDialogButtonLabel "Open"
      return Nothing
    else do
      GI.Gtk.labelSetText fileChooserDialogButtonLabel fileName
      return $ Just fileName

-- | Sets the cursor to the window
showSetCursor::GI.Gtk.Window -> Maybe Text -> IO ()
showSetCursor mainWindow Nothing =
  fromJust <$> GI.Gtk.widgetGetWindow mainWindow >>=
  flip GI.Gdk.windowSetCursor (Nothing :: Maybe GI.Gdk.Cursor)
showSetCursor mainWindow (Just cursor) = do
  gdkWindow <- fromJust <$> GI.Gtk.widgetGetWindow mainWindow
  _cursor <- drawCursor gdkWindow cursor
  GI.Gdk.windowSetCursor gdkWindow _cursor

-- | Draws the cursor
drawCursor::GI.Gdk.Window -> Text -> IO (Maybe GI.Gdk.Cursor)
drawCursor mainWindow cursor = do
  GI.Gdk.windowGetDisplay mainWindow >>=
    flip GI.Gdk.cursorNewFromName cursor

-- | Checks if video is running or paused
checkIsRunning::GI.Gtk.Button -> IO Bool
checkIsRunning playPauseButton =
  GI.Gtk.buttonGetImage playPauseButton >>= fetchimage >>= fetchname >>= fetchchecked
  where
    fetchimage::Maybe GI.Gtk.Widget -> IO (Maybe GI.Gtk.Image)
    fetchimage Nothing       = return Nothing
    fetchimage (Just widget) = GI.Gtk.castTo GI.Gtk.Image widget

    fetchname::Maybe GI.Gtk.Image -> IO (Maybe Text)
    fetchname Nothing        = return Nothing
    fetchname (Just image)   = Just . Data.Text.strip . Data.Text.toLower <$> GI.Gtk.widgetGetName image

    fetchchecked::Maybe Text -> IO Bool
    fetchchecked Nothing         = return False
    fetchchecked (Just text)     = return ("pause-image" == text)

-- | Sets up the play and pause button
setButton::GI.Gtk.Button ->  GI.Gtk.Image ->  GI.Gtk.Image ->  Bool -> IO ()
setButton playPauseBn _ pauseImage True = do
  GI.Gtk.buttonSetImage playPauseBn (Just pauseImage)
  GI.Gtk.widgetSetTooltipText playPauseBn (Just "Pause Video")
setButton playPauseBn playImage _ False = do
  GI.Gtk.buttonSetImage playPauseBn (Just playImage)
  GI.Gtk.widgetSetTooltipText playPauseBn (Just "Play Video")

-- | resetting routines
resetguiField::FO.GUIField -> IO ()
resetguiField FO.GUIField {
     FO.mainWindow            = mainWindow
   , FO.infoLabel             = infoLabel
   , FO.videolocation         = videolocation
   , FO.fileSelectorBnLabel   = fileSelectorBnLabel
   , FO.fileSelectorLabel     = fileSelectorLabel
   , FO.fileSelectorDialog    = fileSelectorDialog
   , FO.fileSelectorWidget    = fileSelectorWidget
   , FO.fileSelectorButton    = fileSelectorButton
   , FO.fileSelectorCancelBn  = fileSelectorCancelBn
   , FO.fileSelectorOpenBn    = fileSelectorOpenBn
   , FO.videoWidget           = videoWidget
   , FO.controlBoxTop         = controlBoxTop
   , FO.controlBoxBottom      = controlBoxBottom
   , FO.seekscale             = seekscale
   , FO.playpauseBn           = playpauseBn
   , FO.playImage             = playImage
   , FO.pauseImage            = pauseImage
   , FO.volumeBn              = volumeBn
   , FO.widthselector         = widthselector
   , FO.speedselector         = speedselector
   , FO.errorDialog           = errorDialog
  } = do
    desiredWidth <- IOUtils.fetchDesiredWidth widthselector mainWindow
    styleContext <- GI.Gtk.widgetGetStyleContext controlBoxBottom
    let windowMinimumWidth = 480
    GI.Gtk.widgetSetSizeRequest mainWindow windowMinimumWidth (-1)
    GI.Gtk.windowUnfullscreen mainWindow
    GI.Gtk.widgetHide videoWidget
    GI.Gtk.widgetHide controlBoxBottom
    GI.Gtk.widgetHide seekscale
    GI.Gtk.widgetHide playpauseBn
    GI.Gtk.widgetShow widthselector
    GI.Gtk.widgetShow fileSelectorButton
    GI.Gtk.widgetShow controlBoxTop
    GI.Gtk.comboBoxSetActive speedselector 1
    GI.Gtk.entrySetText videolocation ""
    GI.Gtk.labelSetText fileSelectorBnLabel "Open"
    GI.Gtk.windowResize mainWindow (fromIntegral desiredWidth :: Int32) 1
    showSetCursor mainWindow Nothing
    setButton playpauseBn playImage pauseImage False