-- | The module to be ultimately executed
module Main where

import Prelude
import MainUtils
import GI.GObject
import Data.Text
import qualified GI.Gtk
import GI.Gst

-- | The main function. It initialises the gtk librariesand builds the glade frame to load the 
-- | display properties of the gui attributes
main :: IO ()
main = do
    gtk_init <- GI.Gtk.init Nothing
    gst_init <- GI.Gst.init Nothing
    
    frameBuilder <- GI.Gtk.builderNewFromFile (pack "Graphics/graphics.glade")
    MainUtils.buildMainFrame frameBuilder