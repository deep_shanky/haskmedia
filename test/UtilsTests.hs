module Main where
import System.Exit (exitFailure)
import TextUtils
import Data.Text
import Test.Hspec
import Test.QuickCheck


main :: IO()
main = hspec $ do
  describe "TextUtils.fileNamefromPath" $ do
    it "returns the file name from a file path" $ do
      TextUtils.fileNamefromPath  (Data.Text.pack ("/home/Haskell/hs.txt")) `shouldBe`  (Data.Text.pack ("hs.txt") :: Data.Text.Text)

  describe "TextUtils.isTextEmpty" $ do
    it "returns the bool value if the text is empty or not" $ do
      TextUtils.isTextEmpty (Data.Text.pack("")) `shouldBe` True

  describe "TextUtils.invalidVideoWidget" $ do
    it "returns the invalidVideoWidget" $ do
      TextUtils.invalidVideoWidget `shouldBe` (Data.Text.pack("invalid-video-widget"))

  describe "TextUtils.enumToInt32" $ do
    it "returns the int32 value corresponding to enum" $ do
      TextUtils.enumToInt32 'a' `shouldBe` 97

  describe "TextUtils.getSomeCommand" $ do
    it "returns xdg-open command" $ do
      TextUtils.getSomeCommand `shouldBe` "xdg-open"

  describe "TextUtils.checkVideoAttributes" $ do
    it "returns the check of default video attributes" $ do
      TextUtils.checkVideoAttributes  (False, 400, 400, "", False, False) `shouldBe` "set improperly"
