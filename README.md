# HaskMedia

HaskMedia is a media player written in Haskell. It allows to play any audio or video with several neccesarry opearations attached to it. You can easily play any media from your system. It comprises of all the basic functionalities required in a media player.

# Abilities of HaskMedia

* Play and pause at any moment
* Seek to a different section
* Alter the speed of the video
  * 0.5
  * 0.75
  * 1.0
  * 1.25
  * 1.5
* Video format accepted : .flv; .3gp; .mkv
* Audio format accepted : .mp3
  

# Getting to install

Firstly, clone the repository :
```
git clone https://deep_shanky@bitbucket.org/deep_shanky/haskmedia.git
cd haskmedia
```
To run the project, the machine shall have GHC and stack package manger setup in it.
Setting up ghc compiler and required GTKs can be done by running the 'make' script as :
```
chmod +x make.sh
./make.sh
```

# Using the Media Player

In order to run the media player, we will use the stack package manager. 
Run the following commands to start the application
```
stack setup
```
```
stack test
```
```
stack ghc -- Main.hs -o main
```
```
./main
``` 